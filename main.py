import numpy
import cv2 as cv
import cv2
import numpy as np
from collections import defaultdict
import matplotlib.pyplot as plt
import chart_recognizer as cr
import scale_recognizer as sr
import utils

filename = '47275507_457246008.jpg'

img = cv2.imread(filename)

img = cr.erode(img, 5)
img = cr.dilate(img, 7)

utils.show_image(img)

contour = cr.find_contour(img)
x, y, w, h = cv2.boundingRect(contour)

arr = cr.contour_coords(contour)

arr = cr.interpolate_array(arr)
arr = cr.running_mean(arr)

sampledFunc = np.zeros(img.shape, np.uint8)

for i in range(0, len(arr)):
    sampledFunc[int(arr[i]), i] = 255

sampledFunc = sampledFunc[y:y + h, x:x + w]

utils.show_image(sampledFunc, 50)

arr = arr[x:x + w]

arr = img.shape[0] - arr

padding, coefficient, _ = sr.recognize_scale(cv2.imread(filename))

arr = (arr - padding) * coefficient


plt.plot(arr)
plt.ylabel('f(x)')
plt.show()
