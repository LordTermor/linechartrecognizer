from collections import defaultdict

import cv2
import numpy as np


def dilate(img, size=1, shape=cv2.MORPH_ELLIPSE):
    element = cv2.getStructuringElement(shape, (2 * size + 1, 2 * size + 1),
                                        (size, size))
    return cv2.dilate(img, element)


def erode(img, size=1, shape=cv2.MORPH_ERODE):
    element = cv2.getStructuringElement(shape, (2 * size + 1, 2 * size + 1),
                                        (size, size))
    return cv2.erode(img, element)


def find_contour(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    _, thresh = cv2.threshold(gray, 125, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # Search for contours and select the biggest one and draw it on mask
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    return max(contours, key=cv2.contourArea)


def contour_coords(cnt):
    d = defaultdict(list)

    for pt in cnt:
        k, v = pt[0]
        if k != 0 and v != 0:
            d[k].append(v)

    arr = np.full(max(d.items(), key=lambda el: el[0])[0] + 1, fill_value=np.nan)

    for i in d.items():
        arr[i[0]] = np.mean(i[1], dtype=np.int32)

    return arr


def interpolate_array(arr):
    mask = np.isnan(arr)
    nonzeroes = lambda z: z.nonzero()[0]

    arr[mask] = np.interp(nonzeroes(mask), nonzeroes(~mask), arr[~mask])

    return arr


def running_mean(arr, size=25):
    return np.convolve(arr, np.ones(size) / size, mode='valid')
