import sys
from PySide2.QtWidgets import QApplication, QDialog, QLineEdit, QPushButton, QMainWindow, QVBoxLayout, QWidget


class Form(QMainWindow):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.setWindowTitle("My Form")
        self.setup_gui()

    def setup_gui(self):
        self.setCentralWidget(QWidget())
        layout = QVBoxLayout()
        layout.addWidget(QPushButton("Кнопка"))
        self.centralWidget().setLayout(layout)


if __name__ == '__main__':
    # Create the Qt Application
    app = QApplication(sys.argv)
    # Create and show the form
    form = Form()
    form.show()
    # Run the main Qt loop
    sys.exit(app.exec_())
