import cv2


def show_image(image_to_show, scale_percent=100, title=""):
    im = cv2.resize(image_to_show, (int(image_to_show.shape[1] * scale_percent / 100),
                                    int(image_to_show.shape[0] * scale_percent / 100)))
    cv2.imshow(title, im)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

