import re
from collections import defaultdict

from pytesseract import pytesseract, Output


def recognize_scale(img):
    d = pytesseract.image_to_data(img, output_type=Output.DICT, lang='rus')
    n_boxes = len(d['level'])
    dd = defaultdict(list)
    for i in range(n_boxes):
        if d['level'][i] != 5 or d['text'][i] == ' ':
            continue

        (x, y, w, h) = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])
        dd[x + w].append({
            'x': x,
            'y': y,
            'w': w,
            'h': h,
            'text': d['text'][i]
        })

    columns = list(dd.items())
    biggest = []
    if len(columns) > 1:
        biggest = max(columns, key=lambda e: len(e[1]))
        columns.remove(biggest)
        for row in biggest[1]:
            for col in columns:
                label = next((x for x in col[1] if (abs(x['y'] - row['y'])) < 2), None)
                if label is None:
                    continue
                if label['x'] > row['x']:
                    row['text'] = row['text'] + label['text']
                    row['w'] = (label['x'] + label['w']) - row['x']
                else:
                    row['text'] = label['text'] + row['text']
                    row['w'] = (row['x'] + row['w']) - label['x']
                    row['x'] = label['x']

    y_scale = biggest[1]

    for i in y_scale:
        i['text'] = re.sub('[^\\d+]', '', i['text'])
        i['y'] = img.shape[0] - i['y'] - (i['h'] / 2)

    scale_pt = y_scale[0]

    delta_y = scale_pt['y'] - y_scale[1]['y']
    delta_val = float(scale_pt['text']) - float(y_scale[1]['text'])

    coefficient = delta_val / delta_y

    padding = scale_pt['y'] - ((float(scale_pt['text']) / delta_val) * delta_y)

    return padding, coefficient, y_scale
